#!/bin/bash
if [ $# -eq 0 ]
  then
    echo "No arguments supplied, assuming username=victorgv and password=12345678"
    username="victorgv"
    password=12345678
elif [ $# -eq 1 ]
  then
    echo "username received as $1, assuming password=12345678"
    username=$1
    password=12345678
elif [ $# -eq 2 ]
  then
    echo "username received as $1, password received as $2"
    username=$1
    password=$2
fi
ACCESS_TOKEN=`curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d "{\"username\":\"$username\",\"password\":\"12345678\"}" 35.198.29.231:82/api/auth_users/login | jq '.id'`
echo $ACCESS_TOKEN
temp="${ACCESS_TOKEN%\"}"
temp="${temp#\"}"
echo $temp
ACCESS_TOKEN="${temp}"
RESPONSE=`curl -X GET "http://35.198.29.231:82/api/auth_users/e928f7b0-cdf2-4d29-a708-7e3aedc7849d?access_token=${ACCESS_TOKEN}"`
if [ `echo $RESPONSE | jq '.username'` == \"$username\" ]
  then
    echo "OK"
else
    echo "ERROR"
    echo `echo $RESPONSE | jq '.username'`
    echo \"$username\"
fi
