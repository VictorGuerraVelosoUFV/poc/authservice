FROM node:lts

WORKDIR /app

RUN apt-get update && \
    apt-get install -y sudo && \
    rm -rf /var/lib/apt/lists/* && \
    adduser --disabled-password --gecos '' --uid 1001 docker && \
    gpasswd -a docker sudo && \
    echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers && \
    chown -R docker:docker /app

USER docker
COPY --chown=docker ./auth /app
RUN npm install
CMD ["npm", "start"]



