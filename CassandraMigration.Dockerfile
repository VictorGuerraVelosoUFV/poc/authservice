FROM cassandra:3

WORKDIR /app

RUN apt-get update && \
    apt-get install -y curl && \
    rm -rf /var/lib/apt/lists/* && \
    adduser --disabled-password --gecos '' --uid 1001 docker && \
    gpasswd -a docker sudo && \
    echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers && \
    chown -R docker:docker /app

USER docker

COPY db-schema/cassandra-migration.cql /app/cassandra-migration.cql

CMD ["cqlsh","-u", "'cassandra'", "-p", "'cassandra'", "-f", "/app/cassandra-migration.cql","cassandra","9042"]
